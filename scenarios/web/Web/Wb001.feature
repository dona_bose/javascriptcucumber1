@author: dona.bose
Feature: Web

@CucumberScenario
Scenario Outline: Wb 001

  Given get "https://qtm4j404releaseprod.atlassian.net/"
  When clear "email.username1"
  And sendkeys "<username>" into "email.username_1"
  And click on "form.formlogin"
  And clear "password.password"
  And sendkeys "<password>" into "password.password"
  And click on "form.formlogin"


Examples:
    |username|password|
    |sameer.gosai@qmetry.com|sameer123#|
    |fddfssdf|dsf|